
export class Recycle{


  seo = false;
  se = false;

  ps = false;
  pc = false;
  sob = false;
  pr = false;

  inse = false;
  vso = false;
  tr = false;
  hb = false;
  pbs = false;

  inte = false;
  iec = false;
  im = false;
  eff = false;
  sgc = false;

  well = false;
  oh = false;
  om = false;
  ol = false;
  hh = false;
  hm = false;
  hl = false;
  asch = false;
  ascm = false;
  ascl = false;

    list = [
        {
          name: 'Social-Emotional Outcomes',
          value: false,
          hasChildren: true,
    
          children: [
            {
              name: 'Social Engagement',
              value: false,
              hasChildren: true,
              children: [
                {
                  name: 'Participate Sports',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Participate Clubs',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Sense of Belonging',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Positive Relationships',
                  value: false,
                  hasChildren: false
                }
              ]
            },
            {
              name: 'Institutional Engagement',
              value: false,
              hasChildren: true,
    
              children: [
                {
                  name: 'Values school Outcomes',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Truancy',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Homework Behaviour',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Positive Behaviour at school',
                  value: false,
                  hasChildren: false
                },
              ]
            },
            {
              name: 'Intellectual Engagement',
              value: false,
              hasChildren: true,
              children: [
    
                {
                  name: 'intellectual Engagement Composite',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Interest and Motivation',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Effort',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Skills (Grades)-Challenge',
                  value: false,
                  hasChildren: false
                }
    
              ]
    
            },
            {
              name: 'wellness',
              value: false,
              hasChildren: true,
              children: [
                {
                  name: 'Optimism High',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Optimism Medium',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Optimism Low',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Happiness High',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Happiness Medium',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Happiness Low',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Academic self-concept High',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Academic self concept Medium',
                  value: false,
                  hasChildren: false
                },
                {
                  name: 'Academic self concept Low',
                  value: false,
                  hasChildren: false
                }
    
              ]
    
            }
          ]
        },
    
        {
          name: 'Academic Outcomes',
          value: false,
          hasChildren: true,
          children: [
            {
              name: 'Language Arts',
              value: false,
              hasChildren: true,
              children: [
                {
                  name: 'English',
                  value: false,
                  hasChildren: false
                }
              ],
            },
            {
              name: 'Maths',
              value: false,
              hasChildren: true,
              children: [
                {
                  name: 'Maths',
                  value: false,
                  hasChildren: false
                }
              ]
            },
            {
              name: 'Science',
              value: false,
              hasChildren: true,
              children: [
                {
                  name: 'science',
                  value: false,
                  hasChildren: false
                }
    
              ]
            }
          ]
        }
    
      ];
}





//find the parents:


// let elemParent=document.getElementById(event.target.id).parentElement.parentElement;

// while(elemParent.title!='card')
// {

//   elemParent=elemParent.parentElement;
 
//  if(elemParent.title==''){ //means collpase div
    
//    elemParent=elemParent.parentElement;
//    continue;
//  }
//   console.log("parent"+elemParent.title);
//   elemParent=elemParent.parentElement;

// }



//find all children 



// let elemChild=document.getElementById(event.target.id).nextElementSibling.nextElementSibling;

//   if(elemChild!=null){
//   elemChild.childNodes.forEach(function(e){
    
//     console.log(e.firstChild.id);
     
//       let innerElemChild =e.firstChild.nextSibling.nextSibling;
//       if(innerElemChild!=null){
//         innerElemChild.childNodes.forEach(function(f){
//           console.log(f.firstChild.id);
         
//         })
//       }
      
     
//   })
// }
// else{
//   console.log(document.getElementById(event.target.id).id);
  
// }

// find sibling 


// let elemChild=document.getElementById(event.target.id).parentElement.nextElementSibling;

//  while(elemChild){

//     console.log(elemChild.firstChild.id);
//     elemChild=elemChild.nextElementSibling;
//  }








  // informSeoChildrenFunc(event) {

  //   if (event.target.checked == true) {
  //     this.seo = true;
  //     this.se = true;
  //     this.ps = true;
  //     this.pc = true;
  //     this.sob = true;
  //     this.pr = true;

  //     this.inse = true;
  //     this.vso = true;
  //     this.tr = true;
  //     this.hb = true;
  //     this.pbs = true;

  //     this.inte = true;
  //     this.iec = true;
  //     this.im = true;
  //     this.eff = true;
  //     this.sgc = true;

  //     this.well = true;
  //     this.oh = true;
  //     this.om = true;
  //     this.ol = true;
  //     this.hh = true;
  //     this.hm = true;
  //     this.hl = true;
  //     this.asch = true;
  //     this.ascm = true;
  //     this.ascl = true;
  //   }

  //   else if (event.target.checked == false) {
  //     this.seo = false;

  //     this.se = false;
  //     this.ps = false;
  //     this.pc = false;
  //     this.sob = false;
  //     this.pr = false;

  //     this.inse = false;
  //     this.vso = false;
  //     this.tr = false;
  //     this.hb = false;
  //     this.pbs = false;

  //     this.inte = false;
  //     this.iec = false;
  //     this.im = false;
  //     this.eff = false;
  //     this.sgc = false;

  //     this.well = false;
  //     this.oh = false;
  //     this.om = false;
  //     this.ol = false;
  //     this.hh = false;
  //     this.hm = false;
  //     this.hl = false;
  //     this.asch = false;
  //     this.ascm = false;
  //     this.ascl = false;

  //   }

  // }

  // execInse(event) {

  // }

  // informSeChildrenFunc(event) {

  //   if (event.target.checked == true) {
  //     this.se = true;
  //     this.ps = true;
  //     this.pc = true;
  //     this.sob = true;
  //     this.pr = true;

  //   }
  //   else if (event.target.checked == false) {
  //     this.se = false;
  //     this.ps = false;
  //     this.pc = false;
  //     this.sob = false;
  //     this.pr = false;

  //   }

  //   this.seo = this.se && this.inte && this.inse && this.well;
  // }

  // changePs(event) {

  //   this.ps = !this.ps;

  //   this.se = this.ps && this.pc && this.sob && this.pr;
  //   this.seo = this.se && this.inse && this.inte && this.well;



  // }

  // changePc() {

  //   this.pc = !this.pc
  //   this.se = this.ps && this.pc && this.sob && this.pr;
  //   this.seo = this.se && this.inse && this.inte && this.well;


  // }

  // changeSob() {

  //   this.sob = !this.sob;
  //   this.se = this.ps && this.pc && this.sob && this.pr;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changePr(event) {

  //   this.pr = !this.pr;
  //   this.se = this.ps && this.pc && this.sob && this.pr;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeInse(event) {
  //   if (event.target.checked == true) {
  //     this.inse = true;
  //     this.vso = true;
  //     this.tr = true;
  //     this.hb = true;
  //     this.pbs = true;

  //   }
  //   else if (event.target.checked == false) {

  //     this.inse = false;
  //     this.vso = false;
  //     this.tr = false;
  //     this.hb = false;
  //     this.pbs = false;

  //   }

  //   this.seo = this.se && this.inse && this.inte && this.well;


  // }

  // changeVso() {

  //   this.vso = !this.vso;

  //   this.inse = this.vso && this.tr && this.hb && this.pbs;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeTr() {
  //   this.tr = !this.tr;
  //   this.inse = this.vso && this.tr && this.hb && this.pbs;
  //   this.seo = this.se && this.inse && this.inte && this.well;

  // }

  // chnageHb() {

  //   this.hb = !this.hb;

  //   this.inse = this.vso && this.tr && this.hb && this.pbs;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changePbs() {
  //   this.pbs = !this.pbs

  //   this.inse = this.vso && this.tr && this.hb && this.pbs;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeInte(event) {

  //   if (event.target.checked == true) {
  //     this.inte = true;
  //     this.iec = true;
  //     this.im = true;
  //     this.eff = true;
  //     this.sgc = true;
  //   }
  //   else if (event.target.checked == false) {
  //     this.inte = false;
  //     this.iec = false;
  //     this.im = false;
  //     this.eff = false;
  //     this.sgc = false;

  //   }

  //   this.seo = this.se && this.inse && this.inte && this.well;

  // }

  // changeIec(event) {

  //   this.iec = !this.iec

  //   this.inte = this.iec && this.im && this.eff && this.sgc;
  //   this.seo = this.se && this.inse && this.inte && this.well;

  // }

  // changeIm(event) {
  //   this.im = !this.im;

  //   this.inte = this.iec && this.im && this.eff && this.sgc;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeEff(event) {
  //   this.eff = !this.eff;

  //   this.inte = this.iec && this.im && this.eff && this.sgc;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // chnangeSgc(event) {
  //   this.sgc = !this.sgc

  //   this.inte = this.iec && this.im && this.eff && this.sgc;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeWellness(event) {

  //   if (event.target.checked == true) {
  //     this.well = true;
  //     this.oh = true;
  //     this.om = true;
  //     this.ol = true;
  //     this.hh = true;
  //     this.hm = true;
  //     this.hl = true;
  //     this.asch = true;
  //     this.ascm = true;
  //     this.ascl = true;
  //   }
  //   else if (event.target.checked == false) {
  //     this.well = false;
  //     this.oh = false;
  //     this.om = false;
  //     this.ol = false;
  //     this.hh = false;
  //     this.hm = false;
  //     this.hl = false;
  //     this.asch = false;
  //     this.ascm = false;
  //     this.ascl = false;

  //   }

  //   this.seo = this.se && this.inse && this.inte && this.well;

  // }

  // changeOh(event) {

  //   this.oh = !this.oh;

  //   this.well = this.oh && this.om && this.ol && this.hh && this.hm && this.hl && this.asch && this.ascm && this.ascl;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeOm(event) {
  //   this.om = !this.om;

  //   this.well = this.oh && this.om && this.ol && this.hh && this.hm && this.hl && this.asch && this.ascm && this.ascl;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeOl(event) {
  //   this.ol = !this.ol

  //   this.well = this.oh && this.om && this.ol && this.hh && this.hm && this.hl && this.asch && this.ascm && this.ascl;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeHh(event) {
  //   this.hh = !this.hh

  //   this.well = this.oh && this.om && this.ol && this.hh && this.hm && this.hl && this.asch && this.ascm && this.ascl;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeHm(event) {
  //   this.hm = !this.hm

  //   this.well = this.oh && this.om && this.ol && this.hh && this.hm && this.hl && this.asch && this.ascm && this.ascl;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeHl(event) {
  //   this.hl = !this.hl

  //   this.well = this.oh && this.om && this.ol && this.hh && this.hm && this.hl && this.asch && this.ascm && this.ascl;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }
  // changeAsch(event) {
  //   this.asch = !this.asch;

  //   this.well = this.oh && this.om && this.ol && this.hh && this.hm && this.hl && this.asch && this.ascm && this.ascl;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeAscm(event) {
  //   this.ascm = !this.ascm

  //   this.well = this.oh && this.om && this.ol && this.hh && this.hm && this.hl && this.asch && this.ascm && this.ascl;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }

  // changeAscl(event) {
  //   this.ascl = !this.ascl;

  //   this.well = this.oh && this.om && this.ol && this.hh && this.hm && this.hl && this.asch && this.ascm && this.ascl;
  //   this.seo = this.se && this.inse && this.inte && this.well;
  // }
