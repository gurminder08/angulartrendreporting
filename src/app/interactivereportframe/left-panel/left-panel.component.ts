import { Component, OnInit} from '@angular/core';
import fontawesome from '@fortawesome/fontawesome';
import { faCaretDown, faCaretRight } from '@fortawesome/fontawesome-free-solid';


@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.css'],
 
})
export class LeftPanelComponent implements OnInit {


  viewCompare: boolean = false;
  viewSort = false;
  viewRefine = false;

  toggleSeo = false;
  toggleSe = false;
  toggleIe = false;
  toggleInte = false;
  toggleWellness = false;
  toggleAo=false;
  toggleLa=false;
  toggleM=false;
  toggleS=false;

  siblings: boolean;

  map = new Map<string, boolean>();

  checks: any = {
    'selectAll':false,
    'Social-Emotional Outcomes': false,
    'Social Engagement': false,
    'Participate Sports': false,
    'Participate Clubs': false,
    'Sense of Belonging': false,
    'Positive Relationships': false,
    'Institutional Engagement': false,
    'Values school Outcomes': false,
    'Truancy': false,
    'Homework Behaviour': false,
    'Positive Behaviour at school': false,
    'Intellectual Engagement': false,
    'intellectual Engagement Composite': false,
    'Interest and Motivation': false,
    'Effort': false,
    'Skills (Grades)-Challenge': false,
    'wellness': false,
    'Optimism High': false,
    'Optimism Medium': false,
    'Optimism Low': false,
    'Happiness High': false,
    'Happiness Medium': false,
    'Happiness Low': false,
    'Academic self-concept High': false,
    'Academic self concept Medium': false,
    'Academic self concept Low': false,
    'Academic Outcomes':false,
    'English':false,
    'Language Arts':false,
    'Maths':false,
    'maths':false,
    'Science':false,
    'science':false
  };


  constructor() {

    fontawesome.library.add(faCaretDown);
    fontawesome.library.add(faCaretRight);

  }

  ngOnInit() {

  }

  updateChecks(event) {

    let baseElement = document.getElementById(event.target.id);
    let baseCheck = event.target.checked;
    this.checks[event.target.id]=baseCheck;
    let elemChild = baseElement.nextElementSibling.nextElementSibling;


    if (elemChild != null) {
      
      let children = elemChild.children;
      for (let i = 0; i < children.length; i++) {

        this.checks[children[i].firstElementChild.id] = baseCheck;
        let innerElemChild = children[i].firstElementChild.nextElementSibling.nextElementSibling;
        if (innerElemChild != null) {

          let innerChildren = innerElemChild.children;
          for (let j = 0; j < innerChildren.length; j++) {

            this.checks[innerChildren[j].firstElementChild.id] = baseCheck;
          }
        }
      }
    }

    else {
      //this.checks[event.target.id] = baseCheck;
    }

    // this.checks["Social Engagement"]=this.checks['Participate Sports']&&this.checks['Participate Clubs']&&this.checks['Sense of Belonging']&&this.checks['Positive Relationships'];
    // this.checks["Institutional Engagement"]=this.checks["Values school Outcomes"]&&this.checks["Truancy"]&&this.checks["Homework Behaviour"]&&this.checks["Positive Behaviour at school"]
    // this.checks["Intellectual Engagement"]=this.checks["intellectual Engagement Composite"]&&this.checks["Interest and Motivation"]&&this.checks["Effort"]&&this.checks["Skills (Grades)-Challenge"]
    // this.checks["wellness"]=this.checks["Optimism High"]&&this.checks["Optimism Medium"]&&this.checks["Optimism Low"]&&this.checks["Happiness High"]&&this.checks["Happiness Medium"]&&this.checks["Happiness Low"]&&this.checks["Academic self-concept High"]&&this.checks["Academic self-concept Medium"]&&this.checks["Academic self-concept Low"]
    // this.checks["Social-Emotional Outcomes"]=this.checks["Social Engagement"]&&this.checks["Institutional Engagement"]&&this.checks["Intellectual Engagement"]&&this.checks["wellness"];
    this.findParent(event.target);


  }

 
  findSibling(baseElement, iter) {


    if (iter == 1)
      this.siblings = baseElement.firstChild.checked;

    let prevElemSibling = baseElement.previousElementSibling;

    while (prevElemSibling) {
      console.log("prev " + prevElemSibling.firstChild.checked);
      this.siblings = this.siblings && prevElemSibling.firstChild.checked;
      prevElemSibling = prevElemSibling.previousElementSibling;
    }

    let nextElemSibling = baseElement.nextElementSibling;

    while (nextElemSibling) {

      console.log("next " + nextElemSibling.firstChild.checked);
      this.siblings = this.siblings && nextElemSibling.firstChild.checked;
      nextElemSibling = nextElemSibling.nextElementSibling;
    }

    console.log(this.siblings);
    return this.siblings;

  }

  findParent(baseElement) {

    let iter = 0;
    let elemParent = baseElement.parentElement.parentElement;
    let mySiblings = baseElement.parentElement;
    while (elemParent.title != 'card') {
      iter++;
      elemParent = elemParent.parentElement;

      console.log("parent" + elemParent.title);
      this.checks[elemParent.title] = this.findSibling(mySiblings, iter);
      mySiblings = elemParent;
      elemParent = elemParent.parentElement;

    }

    // this.map.forEach((value:boolean,key:string) => {
    //     console.log(key,value);
    // });

    //console.log(this.map.values());//new

  }

}