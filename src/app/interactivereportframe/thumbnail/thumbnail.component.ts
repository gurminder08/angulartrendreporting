import { Component, OnInit, Output, Input } from '@angular/core';

import fontawesome from '@fortawesome/fontawesome';
import { faQuestionCircle } from '@fortawesome/fontawesome-free-solid';
import { faQuestion } from '@fortawesome/fontawesome-free-solid'
import { faCaretUp } from '@fortawesome/fontawesome-free-solid'
import * as c3 from 'c3';
import * as d3 from 'd3';
import { color } from 'd3';
import { format } from 'url';

@Component({
  selector: 'app-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: ['./thumbnail.component.css']
})
export class ThumbnailComponent implements OnInit {

  @Input('thumbnailObject') thumbnail: { topic: string, diff: string, li1: string, li2: string, li3: string, p1: string, p2: string, p3: string, id: string };
  // class: {cursor:String,'box-shadow':string,opacity:string,zoom:string,filter:string};


  constructor() {

    fontawesome.library.add(faQuestion);
    fontawesome.library.add(faQuestionCircle);
    fontawesome.library.add(faCaretUp);
  }

  ngOnInit() {

  }

  ngAfterViewInit() {


    let chart1 = c3.generate({
      bindto: '#thumbnailid1',

      size:{
        width: 100,
        height: 80
      },

      data: {
        x: 'x',

        

        columns: [

          ['x', '2017-May', '2017-Sept','2017-June', '2017-July'],
          ['HappinessHigh', 30, 250,45,80,80]

        ],

        color: function (color, d) {

          if (d.index != undefined) {
            if (d.index === 1)
              return "#FF4500";
            if (d.index === 0)
              return "#377E9C";
          }
          return color;

        }

      },
      legend:
      {
        show: false
      },

      axis: {
        x: {
          type: 'categorized',
          show: false,

        },
        y:
        {
          show: false
        },

        y2: {
          max: 260,
          min: 25,
          show: true,
          inner: true,

          tick: {
            count: 2,
            // format: function(d){
            //   let format= d3.format("%");
            //   return format(d)
            // }
          }
        }


      }
    });

    

  }

  // toggleClass($event) {
  //   this.class = $event.type == 'mouseover' ? {cursor: 'pointer',"box-shadow":'3px 3px 15px #666',opacity:'1',zoom:'1.05',filter:'alpha(opacity=100)'} : {cursor:'auto',"box-shadow":'',opacity:'',zoom:'',filter:''};
  //   console.log(this.class);
  // }

}
