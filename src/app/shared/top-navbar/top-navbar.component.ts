import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import fontawesome from '@fortawesome/fontawesome';


@Component({
  selector: 'top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.css']
})
export class RightPanelComponent implements OnInit {

  viewButton = false;
  viewDropdown1: boolean;
  viewDropdown2: boolean;
  viewDropdown3: boolean;
  viewDropdown4: boolean;
  viewDropdown5: boolean;


  constructor(private el: ElementRef) {

    fontawesome.library.add();
  }

  ngOnInit() {
  }

  viewdd1Change(event) {
    this.viewDropdown1 = event;
  }

  viewdd2Change(event) {
    this.viewDropdown2 = event;
  }

  viewdd3Change(event) {
    this.viewDropdown3 = event;
  }

  viewdd4Change(event) {
    this.viewDropdown4 = event;
  }

  viewdd5Change(event) {
    this.viewDropdown5 = event;
  }

}
