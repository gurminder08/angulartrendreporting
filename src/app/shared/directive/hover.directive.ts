import { Directive, ElementRef, HostListener, Output, EventEmitter, Input } from '@angular/core';

@Directive({
  selector: '[appHover]'
})
export class HoverDirective {


  @Input() viewDropd: boolean = false;
  @Output() viewDropdown: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private el: ElementRef) {
  }

  @HostListener('mouseenter') onMouseenter() {
    this.hover(this.viewDropd);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.hover(this.viewDropd);
  }

  private hover(viewDropdown: boolean) {
    this.viewDropd = !viewDropdown;
    this.viewDropdown.emit(this.viewDropd);
  }

}
