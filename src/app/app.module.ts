import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { ThumbnailComponent } from './interactivereportframe/thumbnail/thumbnail.component';
import { RightPanelComponent } from './shared/top-navbar/top-navbar.component';
import { HoverDirective } from './shared/directive/hover.directive';
import { LeftPanelComponent } from './interactivereportframe/left-panel/left-panel.component';
import { HttpClientModule } from '@angular/common/http';
import { GeneralFormComponent } from './intereactivestudentsurveyreport/general-form/general-form.component';
import { ComparisonFormComponent } from './intereactivestudentsurveyreport/comparison-form/comparison-form.component';
import { DrilldownComponent } from './intereactivestudentsurveyreport/drilldown/drilldown.component'




@NgModule({
  declarations: [
    AppComponent,
    ThumbnailComponent,
    RightPanelComponent,
    HoverDirective,
    LeftPanelComponent,
    GeneralFormComponent,
    ComparisonFormComponent,
    DrilldownComponent,


  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]

})

export class AppModule { }
