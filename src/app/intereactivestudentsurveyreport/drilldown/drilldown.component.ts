import { Component, OnInit } from '@angular/core';
import fontawesome from '@fortawesome/fontawesome';
import { faCaretDown, faCaretRight } from '@fortawesome/fontawesome-free-solid';

@Component({
  selector: 'app-drilldown',
  templateUrl: './drilldown.component.html',
  styleUrls: ['./drilldown.component.css']
})
export class DrilldownComponent implements OnInit {


  toggleSex = false;
  toggleGrade = false;
  toggleImmiStatus = false;
  toggleAboriginal = false;

  
  sexes=[
    {
      type:"Male",
      id:"male"
    },
    {
      type:"Female",
      id:"female"
    },
    {
      type:"Both",
      id:"both"
    }
  ];



  checks:any=
    {
      
      'grade':false,
      'four':false,
      'six':false,
      'immiStatus':false,
      'nonImmi':false,
      'lessthan5':false,
      'greaterthan5':false,
      'aboriginal':false,
      'aborig':false,
      'nonAborig':false

    };

  constructor() {
    fontawesome.library.add(faCaretDown);
    fontawesome.library.add(faCaretRight);
  }

  ngOnInit() {
  }

  toggleMaleFemale(event,sex){
    console.log(sex.type);
  }

  updateChecks(event) {

    let baseElement = document.getElementById(event.target.id);
    let baseCheck = event.target.checked;
    this.checks[event.target.id] = baseCheck;
    let elemChild = baseElement.nextElementSibling.nextElementSibling;
   

    if (elemChild != null) {
      
      elemChild=elemChild.nextElementSibling;
      let children = elemChild.children;
     
      for (let i = 0; i < children.length; i++) {

        this.checks[children[i].firstElementChild.id] = baseCheck;
       
        let innerElemChild = children[i].firstElementChild.nextElementSibling.nextElementSibling;
        if (innerElemChild != null) {

          let innerChildren = innerElemChild.children;
          for (let j = 0; j < innerChildren.length; j++) {

            this.checks[innerChildren[j].firstElementChild.id] = baseCheck;
          }
        }
      }
    }

    console.log(this.checks[event.target.id]);
    this.checks["immiStatus"]=this.checks["nonImmi"]&&this.checks["lessthan5"]&&this.checks["greaterthan5"];
    this.checks["grade"]=this.checks["four"]&&this.checks["five"]&&this.checks["six"];
    this.checks["aboriginal"]=this.checks["nonAborig"]&&this.checks["aborig"];

  }

}
